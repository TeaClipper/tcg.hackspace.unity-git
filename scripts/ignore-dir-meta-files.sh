#!/bin/bash

if ! [ -d ".git" ]; then
    echo "Not in a git repository" >&2
    exit 1
fi

SCRIPT_DIR=$(cd "${0%/*}" && pwd)


if ! [ -f ".gitignore" ]; then
	touch .gitignore
	echo "No .gitignore exists"
else
	echo ".gitignore already exists"
fi

cat <<EOT >> .gitignore


# Ignore: name DOT meta
*.meta

# Include: name DOT extension DOT meta
!*.*.meta
EOT

echo "done"
